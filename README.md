## Aim of this repository

Add a virtual environment to use it to work on the project.

## Virtualenv context

Virtualenv is a tool to create isolated Python environments. Since Python 3.3, a subset of it has been integrated into the standard library under the venv module.

Don't put project files in the virtual environment. Virtual environment just intended to separate out the packages dependencies and the versions which will be used for the project.

## Install virtualenv

In a linux terminal, write:

```ruby
pip install virtualenv
```

## Create a virtualenv

In a linux terminal, go to the the folder where you want to implement the virtualenv

Then, to have a virtualenv named venv, write:

```ruby
virtualenv venv
```

## Basic usage

### Start using the virtualenv

In a linux terminal, write:

```ruby
source venv/bin/activate
```

When the virtualenv is activate, its name shows up on the linux terminal to notice you that it is activated.

From now, all the package you install, using pip, will be install on the virtualenv.

### End using the virtualenv

In a linux terminal, write:

```ruby
deactivate
```

## Remove a virtualenv

Just remove the folder. In a linux termial, write:

```ruby
rm -rf venv
```

## Go further in the virtualenv usage

### pip freeze > requirements.txt

Create a file requirements.txt which contains a list of each package in the actual environment with their version.

### pip list

See the list of each package installed.

### pip install -r requirements.txt

Easier way to install the usefull packages with same version.

### virtualenv -p /usr/bin/python2.6 venv

Create a virtualenv venv with the python version specified that is python2.6
