from setuptools import setup

setup(
    name = 'TestSimpleCalculatorEtienneEngel',
    version = '0.0.2',
    author = 'Etienne Engel',
    packages = ['Package/SubPackage', 'Package/Test'],
    description = '''TestSimpleCalculator is a simple package \
    in order to make some test on packaging principles in Python''',
    license = 'GNU GPLv3',
    python_requires = '>=3.4',
)
